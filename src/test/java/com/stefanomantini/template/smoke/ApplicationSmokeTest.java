//package com.stefanomantini.template.smoke;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.http.client.SimpleClientHttpRequestFactory;
//import org.springframework.web.client.RestTemplate;
//import java.net.URL;
//
//import static org.junit.Assert.assertTrue;
//
//public class ApplicationSmokeTest {
//    private int port = 8080;
//    private RestTemplate template;
//    private URL base;
//
//    @Before
//    public void setUp() throws Exception {
//        this.base = new URL("http://localhost:" + port + "/");
//        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
//        template = new RestTemplate(requestFactory);
//    }
//
//    @Given
//    public void test_is_server_up() {
//        System.out.println(template.getForEntity(base+"/health", String.class));
//        assertTrue(template.getForEntity(base + "/health", String.class).getStatusCode().is2xxSuccessful());
//
//    }
//
//
//}
