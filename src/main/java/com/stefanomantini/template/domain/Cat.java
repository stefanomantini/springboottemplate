package com.stefanomantini.template.domain;

import lombok.Getter;
import lombok.Setter;

public class Cat {

  @Getter @Setter public String name;

  public Cat(String name) {
    this.name = name;
  }

}
