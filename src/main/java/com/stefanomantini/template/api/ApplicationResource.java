package com.stefanomantini.template.api;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ApplicationResource {

  Logger log = LoggerFactory.getLogger(ApplicationResource.class);

  @Value("${app.name}")
  private String name;

  @Value("${app.version}")
  private String version;

  @Value("${app.artifact}")
  private String artifactid;

  @Value("${app.buildTimestamp}")
  private String buildTimestamp;

  @RequestMapping(
      method = {RequestMethod.GET},
      produces = "application/json",
      value = {"/version"} )
  public String getVersion() {
    log.trace(name, version, artifactid, buildTimestamp);
    Gson gson = new Gson();
    Map<String, String> versionDetailMap = new HashMap<>();
    versionDetailMap.put("name", name);
    versionDetailMap.put("version", version);
    versionDetailMap.put("artifactid", artifactid);
    versionDetailMap.put("buildTimestamp", buildTimestamp);
    log.debug(versionDetailMap.toString());
    return gson.toJson(versionDetailMap);
  }
}
